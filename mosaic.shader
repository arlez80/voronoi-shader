/*
	割れたガラスシェーダー by あるる（きのもと 結衣）
	Broken Glass Effect Shader by @arlez80

	MIT License
*/

shader_type canvas_item;
render_mode unshaded;

uniform float scale = 20.0;
uniform float shift = 0.05;
uniform float seed = 0.0;

vec2 random( vec2 pos )
{ 
	return fract(
		sin(
			vec2(
				dot(pos, vec2(12.9898,78.233))
			,	dot(pos, vec2(-148.998,-65.233))
			)
		) * 43758.5453
	);
}

void fragment( )
{
	vec2 v = UV * scale + vec2( seed, seed );
	vec2 v_floor = floor( v );
	vec2 v_fract = fract( v );

	vec2 min_p = vec2( 0.0, 0.0 );
	float min_dist = 2.0;

	for( int y = -1; y <= 1; y ++ ) {
		for( int x = -1; x <= 1; x ++ ) {
			vec2 n = vec2( float( x ), float( y ) );
			vec2 p = random( v_floor + n );
			float d = distance( v_fract, p + n );

			min_p = mix( min_p, p, float( d < min_dist ) );
			min_dist = min( min_dist, d );
		}
	}

	// thanks advice to reddit user u/e344fde6bf
	COLOR = textureLod( SCREEN_TEXTURE, SCREEN_UV + min_p * shift, 0.0 );
}
